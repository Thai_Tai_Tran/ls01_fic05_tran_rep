import java.util.Scanner;
public class Utils {
	//Methode 1
	public static double fahrkartenbestellungErfassen() {
		
		Scanner tastatur = new Scanner(System.in);
		int antwortG;
		int antwortWt=0;
		int anzahlTickets;
		double preisTicket ;
		double zuZahlenderBetrag=0;
		boolean isValid;
		
		//Dialog Auswahl
		System.out.print("Servus, Wie geht's Ihnen?\n");
		
		do {
			System.out.println("1 - Ja moin, super und Ihnen?");
			System.out.println("2 - Ich will keinen Smalltalk. Ich WILL ein Ticket!");
			antwortG = tastatur.nextInt();
			//g�ltige Antwort �berpr�fen
			if(antwortG > 2 || antwortG  < 1){
			//if (antwortG != 2 || antwortG  != 1){
				System.out.println("Wie bitte?" );
				isValid = false;
			}  else isValid = true;
			
		} while( !isValid );		
		//Dialog freundlich Antwort 1
		if(antwortG==1) {
			System.out.println("Bestens, dankesch�n! Da hat jemand Manieren!\n");
			for (int i = 0; i < 3; i++)
			{
				try {
					Thread.sleep(450);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("W�hlen Sie bitte ihr Ticket.");
			System.out.println("1 - Einzelticket");
			System.out.println("2 - Familienticket f�r 4 Personen");
			System.out.println("3 - Gruppenticket f�r 10 Personen");

			int ticketArt= tastatur.nextInt();


			switch (ticketArt){
			case 1:
				preisTicket= 2.00;
				System.out.print("Das Ticket kostet 2,00 Euro\n");
				break;
			case 2:
				preisTicket= 5.00;
				System.out.print("Das Ticket kostet 6,00 Euro.\n");
				break;
			case 3:
				preisTicket= 13.00;
				System.out.print("Das Ticket kostet 13,00 Euro.\n");
				break;
			default:
				preisTicket=0.00;
			}

			System.out.print("Wieviele Tickets m�chten Sie kaufen? ");
			anzahlTickets = tastatur.nextInt();
			zuZahlenderBetrag += anzahlTickets * preisTicket;
			
			//Loop Ticket Auswahl
			System.out.println("\nWollen Sie weitere Tickets kaufen? \n 1 - ja\n 2 - nein ");
			antwortWt = tastatur.nextInt();
			//Loop Ticket
			while(antwortWt==1) {

				System.out.println("W�hlen Sie ihr Ticket.");
				System.out.println("1 - Einzelticket");
				System.out.println("2 - Familienticket f�r 4 Personen");
				System.out.println("3 - Gruppenticket f�r 10 Personen");

				int ticketArtl= tastatur.nextInt();

				switch (ticketArtl){
				case 1:
					preisTicket= 2.00;
					System.out.print("Das Ticket kostet 2,00 Euro\n");
					break;
				case 2:
					preisTicket= 5.00;
					System.out.print("Das Ticket kostet 5,00 Euro.\n");
					break;
				case 3:
					preisTicket= 13.00;
					System.out.print("Das Ticket kostet 13,00 Euro.\n");
					break;
				default:
					preisTicket=0.00;
				}

				System.out.print("Wieviele Tickets m�chten Sie? ");
				anzahlTickets = tastatur.nextInt();
				zuZahlenderBetrag += anzahlTickets * preisTicket;

				//Loop Ticket Auswahl
				System.out.println("Wollen Sie weitere Tickets kaufen \n 1 - ja\n 2 - nein ");
				antwortWt = tastatur.nextInt();

			}
		}
		// Dialog unfreundlich Antwort 2
		else if(antwortG==2) {
			System.out.println("Ich bin darauf programmiert freundlich zu sein... Sie sind es anscheinend nicht!");
			for (int i = 0; i < 5; i++)
			{
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("Haben Sie noch eine Chance verdient?");
			for (int i = 0; i < 5; i++)
			{
				System.out.print(".\n");
				try {
					Thread.sleep(500);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
			System.out.println("Beep Boop Bob, der Computer sagt nein!");
			System.exit(0);
		}

		return zuZahlenderBetrag;
	}
	//Methode 2
	public static double fahrkartenBezahlen(double betrag) {
		Scanner tastatur = new Scanner(System.in);
		double eingezahlterGesamtbetrag;
		double zuZahlenderBetrag = betrag;
		double eingeworfeneM�nze;

		eingezahlterGesamtbetrag = 0.0;
		while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
		{
			System.out.printf("Bezahlen Sie bitte noch %.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag)).print(" Euro \n");
			System.out.print("Eingabe (mind. 5Ct, h�chstens 2 Euro): ");
			eingeworfeneM�nze = tastatur.nextDouble();
			eingezahlterGesamtbetrag += eingeworfeneM�nze;
		}
		return eingezahlterGesamtbetrag;

	}
	//Methode 3
	public static void fahrkartenAusgeben() {
		System.out.println("\n Ihre Bestellung  wird extra f�r SIE ausgedruckt");
		for (int i = 0; i < 8; i++)
		{
			System.out.print("=");
			try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		System.out.println("\n\n");
	}
	//Methode 4
	public static void rueckgeldAusgeben(double betrag, double gesamtBetrag) {
		double r�ckgabebetrag;
		double zuZahlenderBetrag = betrag;
		double eingezahlterGesamtbetrag = gesamtBetrag;

		r�ckgabebetrag = eingezahlterGesamtbetrag - zuZahlenderBetrag;
		if(r�ckgabebetrag > 0.0)
		{
			System.out.printf("Der R�ckgabebetrag in H�he von %.2f" , r�ckgabebetrag) .print (" EURO ");
			System.out.println("wird in folgenden M�nzen ausgezahlt:");

			while(r�ckgabebetrag >= 2.0) // 2 EURO-M�nzen
			{
				System.out.println("2 EURO");
				r�ckgabebetrag -= 2.0;
			}
			while(r�ckgabebetrag >= 1.0) // 1 EURO-M�nzen
			{
				System.out.println("1 EURO");
				r�ckgabebetrag -= 1.0;
			}
			while(r�ckgabebetrag >= 0.5) // 50 CENT-M�nzen
			{
				System.out.println("50 CENT");
				r�ckgabebetrag -= 0.5;
			}
			while(r�ckgabebetrag >= 0.2) // 20 CENT-M�nzen
			{
				System.out.println("20 CENT");
				r�ckgabebetrag -= 0.2;
			}
			while(r�ckgabebetrag >= 0.1) // 10 CENT-M�nzen
			{
				System.out.println("10 CENT");
				r�ckgabebetrag -= 0.1;
			}
			while(r�ckgabebetrag >= 0.05)// 5 CENT-M�nzen
			{
				System.out.println("5 CENT");
				r�ckgabebetrag -= 0.05;
			}
		}

		System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
				"vor Fahrtantritt entwerten zu lassen!\n"+
				"Bis zum n�chsten Mal und gute Fahrt! ;)");
	}
	//Gesamtmethode - 4 Teilmethoden zusammengefasst
	public static void fahrkartenautomat(){
	double test = fahrkartenbestellungErfassen();
	double test2 = fahrkartenBezahlen(test);
	fahrkartenAusgeben();
	rueckgeldAusgeben(test, test2);
	}
}
