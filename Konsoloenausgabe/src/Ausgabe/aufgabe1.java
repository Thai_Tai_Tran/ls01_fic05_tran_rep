package Ausgabe;

public class aufgabe1 {
	public static void main(String[] args) {
	//Aufgabe 1
	System.out.print("   **   \n"+
					 "*      *\n"+
					 "*      *\n"+
					 "   **\n");
	//Aufgabe 2
	int name0 = 0;
	int sum0 =1 ;
	int name1 = 1;
	int number1=1;
	int sum1 =1 ;
	System.out.println(name0 +"!   "+"=" + "                   "+"="+sum0);
	System.out.println(name1 +"!   "+"=" + number1+"                  "+"="+sum1);

	//Aufgabe 3
	String fahrenheit= "Fahrenheit";
	String celsius= "Celsius";
	System.out.printf("%-11s|%10s\n", fahrenheit,celsius);
	System.out.println("----------------------");
	System.out.printf("%+-11d|%+10.2f\n", -20,-28.8889);
	System.out.printf("%+-11d|%+10.2f\n", -10,-23.3333);
	System.out.printf("%+-11d|%+10.2f\n", 0,-17.7778);
	System.out.printf("%+-11d|%+10.2f\n", 20,-6.6667);
	System.out.printf("%+-11d|%+10.2f\n", 30,-1.1111);	

	}
}
